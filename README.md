This Plugin is intended for use with the Escrgot IM network at https://escargot.chat

Compared to libpurple's stock plug-in:

 * Faster log-in
 * Fewer crashes
 * Fewer connection issues
 * Support for direct file transfers
 * Support for winks (animoticons) (view-only) (Pidgin)
 * Support for Plus! sounds (receive-only)
 * Option to hide Plus! tags

Other features (which the stock plug-in also has):

 * No timeout issues
 * Server-side storage for display names (private alias)
 * Support for personal status messages
 * Support for offline messaging
 * Send custom emoticons (Pidgin >= 2.5)
 * Support for handwritten messages (read-only)
 * Support for voice clips (receive-only)

Future plans:

 * MSNP18 support

For questions, comments or patches contact:
http://groups.google.com/group/msn-pecan

More information in the home page:
http://code.google.com/p/msn-pecan/
